interface People {
    name: string;
    age: number;
}

interface Organization {
    name: string;
    address: string;
}

export { Organization, People };
