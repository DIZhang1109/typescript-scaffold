import { Organization, People } from "./types";

function sum(a: number, b: number) {
    return a + b;
}

function deduct(a: number, b: number) {
    return a - b;
}

function getPeople(people: People) {
    return `Name: ${people.name}; Age: ${people.age}`;
}

function getOrganization(organization: Organization) {
    return `Name: ${organization.name}; Address: ${organization.address}`;
}

export { deduct, getOrganization, getPeople, sum };
