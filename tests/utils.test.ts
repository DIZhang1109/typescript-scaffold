import chai from "chai";

import { deduct, getOrganization, getPeople, sum } from "../src";

const { expect } = chai;

describe("Unit Test", () => {
    const a = 10;
    const b = 20;

    it("should sum 2 numbers correctly", () => {
        const result = sum(a, b);
        expect(result).to.be.equal(30);
    });

    it("should deduct 2 numbers correctly", () => {
        const result = deduct(a, b);
        expect(result).to.be.equal(-10);
    });

    it("should print a person's details", () => {
        const people = { name: "Di Zhang", age: 1 };
        const result = getPeople(people);
        expect(result).to.be.equal("Name: Di Zhang; Age: 1");
    });

    it("should print an organization's details", () => {
        const organization = { name: "Wellington", address: "New Zealand" };
        const result = getOrganization(organization);
        expect(result).to.be.equal("Name: Wellington; Address: New Zealand");
    });
});
